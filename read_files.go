package main

import (
    "fmt"
    "io"
    "os"
    "log"
)

func sumArrayInteger(arrayIntegers []int) int {  
	sum := 0 
	for _, number := range arrayIntegers {  
		sum += number  
	}  
	return sum  
}

func getNameFiles() []string {
	var filesName []string
	files, err := os.ReadDir("./files")
    if err != nil {
        log.Fatal(err)
    }

    for _, file := range files {
    	filesName = append(filesName, file.Name())
    }

    return filesName
}

func readFile(fileName string) int {
    file, err := os.Open(fileName)

	if err != nil {
	    fmt.Println(err)
	    os.Exit(1)
	}

    var number int
    var numbers []int

    for {
			_, err := fmt.Fscanf(file, "%d\n", &number)
             if err != nil {
				if err == io.EOF {
				    break
				}
				fmt.Println(err)
				os.Exit(1)
             }
        numbers = append(numbers, number)
    }

    return sumArrayInteger(numbers)
}

func main() {
	filesToBeSum := getNameFiles();
	var numbers []int

	for _, file := range filesToBeSum {
		numbers = append(numbers, readFile("files/"+file))
    }

    fmt.Println(sumArrayInteger(numbers))
}